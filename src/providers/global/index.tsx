import {
  createContext,
  useContext,
  useMemo,
  useCallback,
  useState,
  useEffect,
} from 'react'
import {notification} from 'antd'
import { ChildrenType, TConsultaCNPJ, TGlobalProvider } from '@/types'
import axios from 'axios'

const initialState: TGlobalProvider = {
  currentPage: 0,
  enterpriseGroup: '',
  enterpriseSubgroup: '',
  isLoading: false,
  setIsLoading: () => {},
  storeInfos: {} as TConsultaCNPJ,
  setStoreInfos: () => { },
  setEnterpriseSubgroup: () => { },
  setEnterpriseGroup: () => { },
  setCurrentPage: () => { },
  desconto: '',
  setDesconto: () => { },
  parcelamento: '',
  setParcelamento: () => { },
  fechamentos: '',
  setFechamentos: () => { },
  handleGetStoreInfos: (cnpj: string) => { },
  nomeFantasia: '',
  setNomeFantasia: () => { },
  nomeProprietario: '',
  setNomeProprietario: () => { },
  emailFiscal: '',
  setEmailFiscal: () => { },
  emailFinanceiro: '',
  setEmailFinanceiro: () => { },
  emailPedidos: '',
  setEmailPedidos: () => { },
  telefone: '',
  setTelefone: () => { },
  celular: '',
  setCelular: () => { },
  grupoEmpresarial: '',
  setGrupoEmpresarial: () => { },
  cep: '',
  setCep: () => { },
  logradouro: '',
  setLogradouro: () => { },
  numero: '',
  setNumero: () => { },
  complemento: '',
  setComplemento: () => { },
  bairro: '',
  setBairro: () => { },
  municipio: '',
  setMunicipio: () => { },
  estado: '',
  setEstado: () => { },
  emailCadastro: '',
  setEmailCadastro: () => { },
  senha: '',
  setSenha: () => { },
  confirmaSenha: '',
  setConfirmaSenha: () => { },
  handleCreateUser: () => { },
  setCnpj: () => {},
  cnpj: '',
  setVoucher: () => {},
  voucher: '',
  setQuantidade: () => {},
  quantidade: '',
  modalType: 0,
  setModalType: () => {},
  isModalOpen: false,
  setIsModalOpen: () => {},  
}

const GlobalContext = createContext(initialState)

function GlobalProvider({ children }: ChildrenType) {

  const [currentPage, setCurrentPage] = useState<number>(0)
  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [nomeFantasia, setNomeFantasia] = useState<string>('')
  const [nomeProprietario, setNomeProprietario] = useState<string>('')
  const [emailFiscal, setEmailFiscal] = useState<string>('')
  const [emailFinanceiro, setEmailFinanceiro] = useState<string>('')
  const [emailPedidos, setEmailPedidos] = useState<string>('')
  const [telefone, setTelefone] = useState<string>('')
  const [celular, setCelular] = useState<string>('')
  const [grupoEmpresarial, setGrupoEmpresarial] = useState<string>('')
  const [cep, setCep] = useState<string>('')
  const [logradouro, setLogradouro] = useState<string>('')
  const [numero, setNumero] = useState<string>('')
  const [complemento, setComplemento] = useState<string>('')
  const [bairro, setBairro] = useState<string>('')
  const [municipio, setMunicipio] = useState<string>('')
  const [estado, setEstado] = useState<string>('')
  const [emailCadastro, setEmailCadastro] = useState<string>('')
  const [senha, setSenha] = useState<string>('')
  const [confirmaSenha, setConfirmaSenha] = useState<string>('')
  const [enterpriseGroup, setEnterpriseGroup] = useState<string>('')
  const [enterpriseSubgroup, setEnterpriseSubgroup] = useState<string>('')
  const [desconto, setDesconto] = useState<string>('')
  const [parcelamento, setParcelamento] = useState<string>('')
  const [fechamentos, setFechamentos] = useState<string>('')
  const [storeInfos, setStoreInfos] = useState<TConsultaCNPJ>({} as TConsultaCNPJ)
  const [cnpj, setCnpj] = useState<string>('')
  const [quantidade, setQuantidade] = useState<string>('')
  const [voucher, setVoucher] = useState<string>('')
  const [modalType, setModalType] = useState<number>(0)
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false)

  const handleGetStoreInfos = useCallback(async (cnpj: string) => {
    setIsLoading(true)
    const url = './api/consultaCnpj'
    const params = { cnpj }
    
    try {
      const response = await axios.post(url, params)
      await setStoreInfos(response.data)
      setIsLoading(false)
    }
    catch (e) {
      setIsLoading(false)
      console.log(e)
    }

  }, [storeInfos])

  const handleCreateUser = useCallback(async () => {
    setIsLoading(true)
    const url = './api/createUser'
    const params = {
      "cnpj": storeInfos.cnpj,
      "nome_fantasia": nomeFantasia,
      "nome_proprietario": nomeProprietario,
      "email_fiscal": emailFiscal,
      "email_financeiro": emailFinanceiro,
      "email_infopedido": emailPedidos,
      "telefone": telefone,
      "celular_principal": celular,
      "cep_entrega": cep,
      "logradouro_entrega": logradouro,
      "numero_entrega": numero,
      "complemento_entrega": complemento,
      "bairro_entrega": bairro,
      "municipio_entrega": municipio,
      "estado_entrega": estado,
      "gruposn": enterpriseGroup !== '' ? "S" : "N",
      "grupo": enterpriseGroup,
      "subgruposn": enterpriseSubgroup !== '' ? "S" : "N",
      "subgrupo": enterpriseSubgroup,
      "email_maiszeiss": emailPedidos,
      "senha": senha,
    }

    try {
      const response = await axios.post(url, params)
      if(response.data.message.toLowerCase() === 'created'){
        notification.success({
          message: null,
          description: "Usuário cadastrado",
          style: {
            fontWeight: '400',
            fontSize: '0.850rem',
            color: '#353D45',
            borderRadius: '0.25rem',
            boxShadow: 'rgba(0, 0, 0, 0.15)',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          },
        })
        setTimeout(() => {
          setStoreInfos({} as TConsultaCNPJ)
          setNomeFantasia('');
          setNomeProprietario('');
          setEmailFiscal('');
          setEmailFinanceiro('');
          setEmailPedidos('');
          setTelefone('');
          setCelular('');
          setGrupoEmpresarial('');
          setCep('');
          setLogradouro('');
          setNumero('');
          setComplemento('');
          setBairro('');
          setMunicipio('');
          setEstado('');
          setEmailCadastro('');
          setSenha('');
          setConfirmaSenha('');
          setEnterpriseGroup('');
          setEnterpriseSubgroup('');
          setDesconto('');
          setParcelamento('');
          setFechamentos('');
          setCurrentPage(0)
        }, 2000)
        setIsLoading(false)

      }else {
        notification.error({
          message: null,
          description: "Erro ao cadastrar usuário",
          style: {
            fontWeight: '400',
            fontSize: '0.850rem',
            color: '#353D45',
            borderRadius: '0.25rem',
            boxShadow: 'rgba(0, 0, 0, 0.15)',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          },
        })
        setIsLoading(false)
      }
    } catch (e) {
      notification.success({
        message: null,
        description: "Erro ao cadastrar usuário",
        style: {
          fontWeight: '400',
          fontSize: '0.850rem',
          color: '#353D45',
          borderRadius: '0.25rem',
          boxShadow: 'rgba(0, 0, 0, 0.15)',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
        },
      })
      setIsLoading(false)
      console.log('erro >>', e)
    }

  }, [bairro, celular, cep, complemento, emailFinanceiro, emailFiscal, emailPedidos, enterpriseGroup, enterpriseSubgroup, estado, logradouro, municipio, nomeFantasia, nomeProprietario, numero, senha, storeInfos.cnpj, telefone, currentPage])

  const value = useMemo(() => ({
    isLoading,
    setIsLoading,
    currentPage,
    enterpriseGroup,
    enterpriseSubgroup,
    setEnterpriseSubgroup,
    setEnterpriseGroup,
    setCurrentPage,
    desconto,
    setDesconto,
    parcelamento,
    setParcelamento,
    fechamentos,
    setFechamentos,
    handleGetStoreInfos,
    storeInfos, setStoreInfos,
    nomeFantasia,
    setNomeFantasia,
    nomeProprietario,
    setNomeProprietario,
    emailFiscal,
    setEmailFiscal,
    emailFinanceiro,
    setEmailFinanceiro,
    emailPedidos,
    setEmailPedidos,
    telefone,
    setTelefone,
    celular,
    setCelular,
    grupoEmpresarial,
    setGrupoEmpresarial,
    cep,
    setCep,
    logradouro,
    setLogradouro,
    numero,
    setNumero,
    complemento,
    setComplemento,
    bairro,
    setBairro,
    municipio,
    setMunicipio,
    estado,
    setEstado,
    emailCadastro,
    setEmailCadastro,
    senha,
    setSenha,
    confirmaSenha,
    setConfirmaSenha,
    handleCreateUser,
    setVoucher,
    voucher,
    setQuantidade,
    quantidade,
    setCnpj,
    cnpj,
    isModalOpen, 
    setIsModalOpen,
    modalType,
    setModalType,
  }), [
    isLoading, 
    setIsLoading,
    currentPage,
    enterpriseGroup, 
    enterpriseSubgroup, 
    setEnterpriseGroup, 
    setCurrentPage, 
    desconto,
    setDesconto,
    parcelamento,
    setParcelamento,
    fechamentos,
    setFechamentos, 
    handleGetStoreInfos, 
    storeInfos, 
    setStoreInfos, 
    nomeFantasia,
    setNomeFantasia,
    nomeProprietario,
    setNomeProprietario,
    emailFiscal,
    setEmailFiscal,
    emailFinanceiro,
    setEmailFinanceiro,
    emailPedidos,
    setEmailPedidos,
    telefone,
    setTelefone,
    celular,
    setCelular,
    grupoEmpresarial,
    setGrupoEmpresarial,
    cep,
    setCep,
    logradouro,
    setLogradouro,
    numero,
    setNumero,
    complemento,
    setComplemento,
    bairro,
    setBairro,
    municipio,
    setMunicipio,
    estado,
    setEstado,
    emailCadastro,
    setEmailCadastro,
    senha,
    setSenha,
    confirmaSenha,
    setConfirmaSenha,
    handleCreateUser,
    setVoucher,
    voucher,
    setQuantidade,
    quantidade,
    setCnpj,
    cnpj,
    isModalOpen, 
    setIsModalOpen,
    modalType,
    setModalType,
  ])

  return (
    <GlobalContext.Provider value={value}>{children}</GlobalContext.Provider>
  )
}

const useGlobal = () => useContext(GlobalContext)

export { GlobalProvider, useGlobal }
