import {
    createContext,
    useContext,
    useMemo,
    useCallback,
    useState,
    useEffect,
} from 'react'

import { ChildrenType, TRegisterProvider } from '@/types'
import { TStoreConditions } from '@/types'
import axios from 'axios'
import { useGlobal } from '../global'

const initialState: TRegisterProvider = {
    codCliente: '',
    setCodCliente: () => {},
    storeConditionsData: {} as TStoreConditions,
    setStoreConditionsData: () => {},
    handleGetStoreConditions: () => {},
}

const RegisterContext = createContext(initialState)

function RegisterProvider({ children }: ChildrenType) {
    const {enterpriseSubgroup, setDesconto, setParcelamento, setFechamentos, fechamentos, desconto, parcelamento} = useGlobal()
    const [codCliente, setCodCliente] = useState<string>('')
    const [storeConditionsData, setStoreConditionsData] = useState<TStoreConditions>({} as TStoreConditions)

    const handleGetStoreConditions = useCallback(async () => {
        const url = './api/groupData'
        try {
            const response = await axios.post(url, {codCli: enterpriseSubgroup})
            setParcelamento(response.data.nome_cond_pgto)
            setDesconto(response.data.desc_tab_desc_sao)
            setFechamentos(response.data.desc_fech_fatura)
            await setStoreConditionsData(response.data)
        }catch(e) {
            console.log(e)
        }
    }, [storeConditionsData, enterpriseSubgroup, fechamentos, desconto, parcelamento])


    const value = useMemo(() => ({ codCliente, setCodCliente, storeConditionsData, setStoreConditionsData, handleGetStoreConditions }), [
        codCliente, setCodCliente, storeConditionsData, setStoreConditionsData, handleGetStoreConditions
    ])

    return (
        <RegisterContext.Provider value={value}>{children}</RegisterContext.Provider>
    )
}

const useRegister = () => useContext(RegisterContext)

export { RegisterProvider, useRegister }
