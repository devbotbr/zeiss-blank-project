import {RegisterFormTemplate} from '@/components'
import { RegisterProvider } from '@/providers/register'

export default function RegisterPage(){
    return (<RegisterProvider>
        <RegisterFormTemplate />
        </RegisterProvider>)
}