import { NextApiRequest, NextApiResponse } from 'next/types'
import Axios from 'axios'

export default async function getGroupData(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  try {
    const url = `https://uwdvs4xpk9.execute-api.us-east-2.amazonaws.com/prd/cadastro/clientes/condicaocomercial/${req.body.codCli}/0`

    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    }

    Axios.defaults.headers.common = {
      'x-api-key': 'W4dW4Gehdj7yUJuDycUeW4dY5hAHnXOX8EnUIsBp',
    }

    const response = await Axios.get(url, config)
    res.status(200)
    res.end(JSON.stringify(response.data))
  } catch (err) {
    res.end(JSON.stringify(err))
  }
}
