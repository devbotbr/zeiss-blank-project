import { NextApiRequest, NextApiResponse } from 'next/types'
import Axios from 'axios'

export default async function consultaCNPJ(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  try {
    const url = `https://kj0kpzwxgl.execute-api.us-east-2.amazonaws.com/dev/consulta/cnpj/rfb/${req.body.cnpj}`

    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    }

    Axios.defaults.headers.common = {
      'x-api-key': 'W4dW4Gehdj7yUJuDycUeW4dY5hAHnXOX8EnUIsBp',
    }

    const response = await Axios.get(url, config)
    res.status(200)
    res.end(JSON.stringify(response.data))
  } catch (err) {
    res.end(JSON.stringify(err))
  }
}
