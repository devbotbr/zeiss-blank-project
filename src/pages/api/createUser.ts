import { NextApiRequest, NextApiResponse } from 'next/types'
import Axios from 'axios'

export default async function createUser(
  req: NextApiRequest,
  res: NextApiResponse,
) {
    const url = `https://kj0kpzwxgl.execute-api.us-east-2.amazonaws.com/dev/cadastro/cliente`
    const params = req.body
    const config = {
      headers: {
        'Content-Type': 'application/json',
        'x-api-key': 'W4dW4Gehdj7yUJuDycUeW4dY5hAHnXOX8EnUIsBp',
      },
    }

  try {
    console.log('params >>', params)
    
    const response = await Axios.post(url, params, config)

    console.log(response.data)
    res.end(JSON.stringify(response.data))
  } catch (err) {
    res.end(JSON.stringify(err))
  }
}
