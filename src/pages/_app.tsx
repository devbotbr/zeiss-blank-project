import '../styles/styles.css'
import { GlobalProvider } from '@/providers/global'
import { GlobalStyle } from '@/styles/globals'
import type { AppProps } from 'next/app'

export default function App({ Component, pageProps }: AppProps) {
  return (<>
    <GlobalProvider>
      <GlobalStyle />
      <title>ZEISS - Lens Whipes</title>
      <Component {...pageProps} />
    </GlobalProvider>
  </>)
}
