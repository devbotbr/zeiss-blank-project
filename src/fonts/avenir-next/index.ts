import { FlavoredFonts, FONT_NAMES } from '../types'
import { CSSFontsAvenir } from './font-faces'

const FONTS: FlavoredFonts = {
  FONT_IMPORTS: CSSFontsAvenir,
  HEADINGS: FONT_NAMES.AVENIR,
  PARAGRAPH: FONT_NAMES.AVENIR,
  DISPLAY: FONT_NAMES.AVENIR,
}

export const AVENIR_FONTS = {
  ...FONTS,
}
