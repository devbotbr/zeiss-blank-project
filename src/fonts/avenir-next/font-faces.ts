import { css } from 'styled-components'
// import AvenirNextBold from './AvenirNextLTPro-Bold.otf'
// import AvenirNextRegular from './AvenirNextLTPro-Regular.otf'
// import AvenirNextItalic from './AvenirNextLTPro-It.otf'
import { FONT_NAMES } from '../types'

export const CSSFontsAvenir = css`
  @font-face {
    font-family: ${FONT_NAMES.AVENIR};
    src: url('./AvenirNextLTPro-Bold.otf') format('otf'),
      url('./AvenirNextLTPro-Regular.otf') format('otf'),
      url('./AvenirNextLTPro-It.otf') format('otf');
  }
`
