import { AVENIR_FONTS } from '@/fonts/avenir-next'
import { ZEISS_FONTS } from '@/fonts/zeiss-frutiger'

export const PROJECT_FONTS = {
  Avenir: AVENIR_FONTS,
  Zeiss: ZEISS_FONTS,
}
