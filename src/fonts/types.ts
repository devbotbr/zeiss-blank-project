import { FlattenSimpleInterpolation } from 'styled-components'

export enum FONT_NAMES {
  AVENIR = 'AvenirNextLTPro',
  ZEISS = 'ZEISSFrutigerNextW1G',
}

export type FlavoredFonts = {
  FONT_IMPORTS: FlattenSimpleInterpolation
  HEADINGS: string
  PARAGRAPH: string
  DISPLAY: string
}
