import { FlavoredFonts, FONT_NAMES } from '../types'
import { CSSFontsZeissFrutiger } from './font-faces'

const FONTS: FlavoredFonts = {
  FONT_IMPORTS: CSSFontsZeissFrutiger,
  HEADINGS: FONT_NAMES.ZEISS,
  PARAGRAPH: FONT_NAMES.ZEISS,
  DISPLAY: FONT_NAMES.ZEISS,
}

export const ZEISS_FONTS = {
  ...FONTS,
}
