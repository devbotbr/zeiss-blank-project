// import { css } from 'styled-components'

// import ZeissFrutigerBold from './ZEISSFrutigerNextW1G-Bold.ttf'

// import ZeissFrutigerRegular from './ZEISSFrutigerNextW1G-Reg.ttf'
// import { FONT_NAMES } from '../types'

// export const CSSFontsZeissFrutiger = css`
//   @font-face {
//     font-family: ${FONT_NAMES.ZEISS};
//     src: url(${ZeissFrutigerBold}) format('ttf'),
//       url(${ZeissFrutigerRegular}) format('ttf');
//   }
// `

import { css } from 'styled-components'
import { FONT_NAMES } from '../types'

export const CSSFontsZeissFrutiger = css`
  @font-face {
    font-family: ${FONT_NAMES.ZEISS};
    src: url('./ZEISSFrutigerNextW1G-Bold.ttf') format('ttf'),
      url('./ZEISSFrutigerNextW1G-Reg.ttf') format('ttf');
  }
`
