import { createGlobalStyle } from 'styled-components'

export const GlobalStyle = createGlobalStyle` * {
  margin: 0px;
  padding: 0px;
  box-sizing: border-box;
  font-size: 16px;
  /* font-family: CSSFontsZeissFrutiger; */
  font-family: zeiss;
  color: #353D45;
}

h1,
h2,
h3,
h4,
h5,
h6,
p {
  margin: 0px;
  padding-top: 4px;
  font-family: zeiss;
}

p{
  margin-top: 0.125rem !important;
}

b {
  font-family: zeissBold;
  padding-top: 4px;
  font-family: zeissBold;
}

body {
  overflow: hidden;
}

`
