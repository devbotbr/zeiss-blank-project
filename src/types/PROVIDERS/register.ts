import { Dispatch, SetStateAction } from "react"
import { TStoreConditions } from "@/types"

export type TRegisterProvider = {
    codCliente: string
    setCodCliente: Dispatch<SetStateAction<string>>
    storeConditionsData: TStoreConditions,
    setStoreConditionsData: Dispatch<SetStateAction<TStoreConditions>>
    handleGetStoreConditions: () => void
}