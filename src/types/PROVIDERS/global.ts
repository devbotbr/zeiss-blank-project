import { Dispatch, SetStateAction } from "react"
import { TConsultaCNPJ } from ".."

export type TGlobalProvider = {
    isLoading: boolean
    setIsLoading: Dispatch<SetStateAction<boolean>>
    storeInfos: TConsultaCNPJ
    setStoreInfos: Dispatch<SetStateAction<TConsultaCNPJ>>
    currentPage: number
    enterpriseGroup: string
    enterpriseSubgroup: string
    desconto: string
    setDesconto: Dispatch<SetStateAction<string>>
    parcelamento: string,
    setParcelamento: Dispatch<SetStateAction<string>>
    fechamentos: string
    setFechamentos: Dispatch<SetStateAction<string>>
    setEnterpriseSubgroup: Dispatch<SetStateAction<string>>
    setEnterpriseGroup: Dispatch<SetStateAction<string>>
    setCurrentPage: Dispatch<SetStateAction<number>>
    handleGetStoreInfos: (cnpj: string) => void
    handleCreateUser: () => void,
    nomeFantasia: string;
    setNomeFantasia: Dispatch<SetStateAction<string>>
    nomeProprietario: string
    setNomeProprietario: Dispatch<SetStateAction<string>>
    emailFiscal: string
    setEmailFiscal: Dispatch<SetStateAction<string>>
    emailFinanceiro: string
    setEmailFinanceiro: Dispatch<SetStateAction<string>>
    emailPedidos: string
    setEmailPedidos: Dispatch<SetStateAction<string>>
    telefone: string
    setTelefone: Dispatch<SetStateAction<string>>
    celular: string
    setCelular: Dispatch<SetStateAction<string>>
    grupoEmpresarial: string
    setGrupoEmpresarial: Dispatch<SetStateAction<string>>
    cep: string
    setCep: Dispatch<SetStateAction<string>>
    logradouro: string
    setLogradouro: Dispatch<SetStateAction<string>>
    numero: string
    setNumero: Dispatch<SetStateAction<string>>
    complemento: string
    setComplemento: Dispatch<SetStateAction<string>>
    bairro: string
    setBairro: Dispatch<SetStateAction<string>>
    municipio: string
    setMunicipio: Dispatch<SetStateAction<string>>
    estado: string
    setEstado: Dispatch<SetStateAction<string>>
    emailCadastro: string
    setEmailCadastro: Dispatch<SetStateAction<string>>
    senha: string
    setSenha: Dispatch<SetStateAction<string>>
    confirmaSenha: string
    setConfirmaSenha: Dispatch<SetStateAction<string>>
    cnpj: string
    setCnpj: Dispatch<SetStateAction<string>>
    quantidade: string
    setQuantidade: Dispatch<SetStateAction<string>>
    voucher: string
    setVoucher: Dispatch<SetStateAction<string>>
    modalType: number 
    setModalType: Dispatch<SetStateAction<number>>
    isModalOpen: boolean
    setIsModalOpen: Dispatch<SetStateAction<boolean>>
}