import { Dispatch, SetStateAction, ReactNode } from 'react'

export type ModalProps = {
  isOpen: boolean
  backgroundModalColor?: string
}

export type TModalOptions = {
  numberOption: number
}
