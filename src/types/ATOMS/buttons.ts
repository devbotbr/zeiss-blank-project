import { ReactNode } from 'react'

export type TPrimaryButton = {
  label: string
  suffixIcon?: ReactNode
  preffixIcon?: ReactNode
  onPressed?: () => void
  disabled?: any
  width?: string
}
