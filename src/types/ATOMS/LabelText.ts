import { ReactNode } from 'react'

export type TLabelText = {
  label: string
}
