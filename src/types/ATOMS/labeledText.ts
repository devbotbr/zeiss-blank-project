export type TLabeledText = {
  title: string
  content: string
}
