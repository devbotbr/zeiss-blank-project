import { Dispatch, ReactNode, SetStateAction } from 'react'

export type Option = {
  label: string
  value: string
}

export type DropdownProps = {
  options: Option[]
  onSelect: (selectedOption: Option) => void
  // eslint-disable-next-line react/require-default-props
  icon?: ReactNode
  placeholder: string
  title?: string
  isFocused?: boolean
  width?: string
  search?: boolean
  optionsOpen?: boolean
  margin?: string
  marginMobile?: string
}

export type TCustomDropdown = {
  search: boolean
  placeholder?: string
  label?: string
  options: any[]
  isFocused?: boolean
  color?: string
  prefixIcon?: ReactNode
  suffixIxon?: ReactNode
  isOpen?: boolean
  margin?: string
  marginMobile?: string
  width?: string
  widthMobile?: string
  state?: Dispatch<SetStateAction<string>>
  id?: string
  defaultValue?: string
  disabled?: boolean
  isVisible?: boolean | string
}

export type TLensServices = {
  nomeservico: string
  codservico: string
}
