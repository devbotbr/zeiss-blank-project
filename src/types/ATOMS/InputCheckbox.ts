import { ReactNode } from 'react'

export type TInputCheckbox = {
  label?: ReactNode | string
  subLabel?: string
  width?: string
  widthMobile?: string
  margin?: string
  marginMobile?: string
  isVisible?: string | boolean
  value?: boolean
  disabled?: boolean
  setStateProp?: (value: boolean) => void
}
