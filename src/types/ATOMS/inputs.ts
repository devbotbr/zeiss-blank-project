import { ReactNode } from "react"

export type MaterialInputType = {
  icon?: ReactNode
  label: string
  rightSideIcon?: ReactNode
  register?: any
  type?: string
  width?: string | number
  placeholder: string
  textArea?: boolean
  color?: string
  isFocused?: boolean
  sufix?: string
  sufixIcon?: ReactNode
  margin?: string
  marginMobile?: string
  defaultValue?: string
  setStateProp?: () => void
  error?: string
  disabled?: boolean
  onChange?: () => void
  onFocusedSetIndex?: () => void
  isVisible?: string | boolean
  mask?: string
}
