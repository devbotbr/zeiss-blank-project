export type TStoreConditions = {
"cod_fech_fatura": string,
  "desc_fech_fatura": string,
  "tab_desc_sao": number | string,
  "desc_tab_desc_sao": string,
  "cond_pgto_sao": number | string,
  "nome_cond_pgto": string,
}