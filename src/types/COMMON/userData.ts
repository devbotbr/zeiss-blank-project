export type TUserData = {
    cnpj: string
    nome_fantasia: string
    nome_proprietario: string
    email_fiscal: string
    email_financeiro: string
    email_infopedido: string
    telefone: string
    celular_principal: string
    cep_entrega: string
    logradouro_entrega: string
    numero_entrega: string
    complemento_entrega: string
    bairro_entrega: string
    municipio_entrega: string
    estado_entrega: string
    gruposn: string
    grupo: string
    subgruposn: string
    subgrupo: string
    email_maiszeiss: string
    senha: string
};
