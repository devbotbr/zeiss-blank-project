export type TConsultaCNPJ = {
    idconsultacnpj: string
    cnpj: string
    cnpjformatado: string
    dataabertura: string 
    razaosocial: string
    nomefantasia: string
    porte: string
    cnaeprincipal: string
    cnaesecundario: Record<string, any> 
    matriz: boolean
    naturezajuridica: string
    logradouro: string
    numero: string
    complemento: string
    cep: string
    bairro: string
    cidade: string
    estado: string
    situacaocadastral: string
    datasituacaocadastral: string 
    motivosituacaocadastral: string
    capitalsocial: string
    qsa: Record<string, any> 
    telefone: string
    efr: string
    email: string
    situacaoespecial: string
    datasituacaoespecial: string | null
    sucesso: string 
    error: string
    datahoraconsulta: string 
    tempoconsulta: string
}