import styled from 'styled-components'

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100vw;
  min-height: 37.75rem;
  height: 37.75rem;
  align-items: center;
  justify-content: center;
  background-color: #ffffff;

  .container {
    width: 19.4375rem;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: center;

    h2 {
      color: #353d45;
      font-size: 2.5rem;
      font-style: normal;
      font-weight: 300;
      line-height: normal;
    }
    h3 {
      color: #828d9e;
      font-size: 1.125rem;
      font-style: normal;
      font-weight: 400;
      line-height: normal;
    }
  }
`
