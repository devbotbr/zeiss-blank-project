import HidePasswordIcon from '@/assets/svg/hidePassword.svg'
import ShowPasswordIcon from '@/assets/svg/showPassword.svg'
import MailIcon from '@/assets/svg/IconMailLogin.svg'
import PasswordIcon from '@/assets/svg/passwordIconLogin.svg'
import { MaterialInput, PrimaryButton } from '@/components'
import * as Styles from './styles'
import { useState } from 'react'
import { useRouter } from 'next/router'

export function LoginBody() {
  const [isPasswordVisible, setIsPasswordVisible] = useState<boolean>(false)
  const router = useRouter()

  return (
    <Styles.Wrapper>
      <form className="container">
        <h2 className="title">Login</h2>
        <h3>Entre com seu e-mail e senha:</h3>
        <MaterialInput
          placeholder="Digite seu e-mail"
          label="E-mail"
          margin="2.5rem 0rem 0rem 0rem"
          icon={<MailIcon />}
          width="19.4375rem"
        />
        <MaterialInput
          placeholder="Digite sua senha"
          label="Senha"
          type={isPasswordVisible ? 'text' : 'password'}
          width="19.4375rem"
          margin="1rem 0rem 2rem 0rem"
          icon={<PasswordIcon />}
          sufixIcon={
            isPasswordVisible ? (
              <HidePasswordIcon
                onClick={() => setIsPasswordVisible(false)}
                style={{ cursor: 'pointer' }}
              />
            ) : (
              <ShowPasswordIcon
                onClick={() => setIsPasswordVisible(true)}
                style={{ cursor: 'pointer' }}
              />
            )
          }
        />
        <PrimaryButton label="Entrar" width="19.4375rem" onPressed={() => router.push('/register')}/>
      </form>
    </Styles.Wrapper>
  )
}
