import { Header, LoginBody } from '@/components'
import * as Styles from './styles'

export function LoginTemplate() {
  return (
    <>
      <Styles.BodyDiv id="bodyDiv" isModalOpen={false}>
        <Header />
        <LoginBody />
      </Styles.BodyDiv>
    </>
  )
}
