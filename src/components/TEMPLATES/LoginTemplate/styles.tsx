import { TScrollTemplate } from '@/types'
import styled from 'styled-components'

export const BodyDiv = styled.div<TScrollTemplate>`
  display: flex;
  flex-direction: column;
  /* width: 100vw; */
  /* height: 100%; */
  height: 100vh;
  /* background-color: #f9f9fa; */
  background-color: #F6F7FA;
  overflow-y: ${(props) => (props.isModalOpen ? 'hidden' : 'scroll')};
  overflow-x: hidden;
  align-items: center;
  padding: 5rem 0rem 2rem;
  .headerDiv {
    margin-top: 13rem;
  }

  .desktop {
    display: none;
  }
  .wrap {
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: flex-start;
    max-width: 90rem;
  }

  @media only screen and (min-width: 1024px) {
    padding-left: 5rem;
    padding-right: 5rem;
    padding-top: 12rem;
    padding-bottom: 8rem;
    overflow-x: hidden !important;
    overflow-y: ${(props) => (props.isModalOpen ? 'hidden' : 'scroll')};

    .headerDiv {
      margin-top: 8rem;
    }
    .desktop {
      display: flex;
      max-width: 100%;
    }
  }
`

export const Wrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 100%;
  /* margin-left: 2rem; */
  max-width: 90rem;
  padding: 0rem;
  overflow-y: hidden;
`
