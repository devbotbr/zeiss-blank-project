import { Header, LensWipesRequestBody, Modal } from '@/components'
import * as Styles from './styles'

export function RegisterFormTemplate() {
  return (
    <>
      <Modal />
      <Styles.BodyDiv id="bodyDiv" isModalOpen={false}>
        <Header />
        <div className="wrap">
          <LensWipesRequestBody />
        </div>
      </Styles.BodyDiv>
    </>
  )
}
