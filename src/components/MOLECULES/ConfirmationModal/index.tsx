import { LabelText, LabeledText, PrimaryButton } from '@/components'
import CloseIcon from '@assets/svg/close.svg'
import * as Styles from './styles'
import { useGlobal } from '@/providers/global'

export function ConfirmationModal() {

    const {setIsModalOpen} = useGlobal()

    return <>
    <Styles.Wrapper>
        <div className='closeDiv' onClick={() => {
            setIsModalOpen(false)
        }}>
        <CloseIcon /> 
        </div>
        <h1>Confirmação do pedido</h1>
        <LabelText label='Revise os dados de entrega e de pedido. Em seguida, clique em ”Fazer pedido”.' />
        <Styles.InfoDrawer>
            <Styles.InfoContainer>
                <h2>Informações da loja</h2>
                <LabeledText title='Nome da ótica' content='Ótica Boa Visão' />
                <LabeledText title='Endereço' content='Rua Visconde de Pirajá, 540 - Loja E - Ipanema, Rio de Janeiro RJ, 22410-002' />
            </Styles.InfoContainer>
            <Styles.InfoContainer2>
                <h2>Pedido</h2>
                <LabeledText title='Pacotes' content='24 pacotes de Lens Wipes' />
                <LabeledText title='Valor total' content='R$ 261,60 + Impostos' />
            </Styles.InfoContainer2>
        </Styles.InfoDrawer>

        <Styles.ModalFooter>
            <PrimaryButton label='Fazer pedido' />
        </Styles.ModalFooter>
    </Styles.Wrapper>
    </>
}