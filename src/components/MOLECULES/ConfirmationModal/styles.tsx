import styled from 'styled-components'

export const Wrapper = styled.div`
    display: flex;
    padding: 2rem 1.5rem 1.5rem;
    flex-direction: column;
    position: relative;
    justify-content: flex-start;
    align-items: flex-start;
    width: auto;
    min-width: 64rem;
    border-radius: 0.125rem;
    background: #FFF;

    .closeDiv {
        cursor: pointer;
        position: absolute;
        top: 1.5rem;
        right: 1.5rem;
    }

    h1 {
        color: #353D45;
        font-size: 1.75rem;
        font-style: normal;
        font-weight: 300;
        line-height: normal;
        margin-bottom: 0rem;
    }
`

export const InfoDrawer = styled.div`
    display: flex;
    width: 100%;
    min-height: 14.625rem;
    align-items: center;
    flex-direction: row;
    flex-shrink: 0;
    border-radius: 0.25rem;
    border: solid 0.0625rem #DDE3ED;
    padding: 1.5rem;
    margin-top: 2rem;
`

export const InfoContainer = styled.div`
    min-width: 47%;
    display: flex;
    flex-direction: column;
    height: 100%;
    border-right: solid 0.0625rem #DDE3ED;    
    padding-right: 1.5rem;
    gap: 1.5rem;
    h2 {
        font-family: zeissBold;
        font-size: 0.75rem;
        color: #353D45;
    }
    `
export const InfoContainer2 = styled.div`
    min-width: 47%;
    display: flex;
    flex-direction: column;
    height: 100%;
    /* border-right: solid 0.0625rem #DDE3ED;     */
    padding-left: 1.5rem;
    gap: 1.5rem;
    h2 {
        font-family: zeissBold;
        font-size: 0.75rem;
        color: #353D45;
    }
`

export const ModalFooter = styled.div`
    position: relative;
    display: flex;
    align-items: center;
    justify-content: center;
    z-index: 35;
    margin-top: 3rem;
    border-radius: 0.125rem;
    bottom: 0rem !important;
    left: 0rem !important;
    min-width: 100%;
    min-height: 7rem;
    background: #FFFFFF;
    box-shadow: 0rem -0.25rm 1rem 0rem rgba(0, 0, 0, 0.06);
`