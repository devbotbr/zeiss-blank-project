import { CustomDropdownSearch, LabelText, LabeledText, MaterialInput, PrimaryButton } from '@/components'
import * as Styles from './styles'
import { useState } from 'react'
import { useGlobal } from '@/providers/global'

export function FormLensWipe() {
    const { cnpj, setCnpj, setQuantidade, quantidade, setVoucher, voucher,
        setIsModalOpen, setModalType,
    } = useGlobal()

    return <Styles.Wrapper>
        <LabelText label='Informações da loja' />
        <CustomDropdownSearch options={['47.440.077/0001-46', '11.111.111/0001']} search label='Loja' placeholder='Selecione o CNPJ' width="24.25rem" margin='1.5rem 0rem 2rem' state={setCnpj} defaultValue={cnpj} />
        <LabelText label='Pedido' />
        <div className='line'>
            <CustomDropdownSearch options={['8 pacotes', '16 pacotes', '24 pacotes', '32 pacotes', '40 pacotes']} search label='Quantidade' placeholder='Selecione o CNPJ' width="24.25rem" margin='0rem 0rem 1.5rem' state={setQuantidade} defaultValue={quantidade} />
            <p className='priceValue'>
                Valor total: <h3>R$<b>261,60 + impostos</b></h3>
            </p>
        </div>
        <MaterialInput label='Voucher' placeholder='Insira o voucher' width="24.25rem" />
        <div className='line2'>
            <PrimaryButton label='Fazer pedido' onPressed={() => {
                setModalType(0)
                setIsModalOpen(true)
            }} />
        </div>
    </Styles.Wrapper>
}