import styled from 'styled-components'

export const Wrapper = styled.div`
  width: 54rem;
  height: max-content;

  .line2 {
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
    width: 100%;
  }

  .line {
    display: flex;
    flex-direction: row;
    align-items: flex-end;
    gap: 1rem;
    .dropdownSpace {
      width: 50%;
    }

    .priceValue {
      height: 1.125rem;
      display: flex;
      flex-direction: row;
      justify-content: flex-start;
      align-items: flex-end;
      color: #828D9E;
      font-size: 0.75rem;
      font-style: normal;
      font-weight: 400;
      line-height: normal;
      margin-bottom: 1.5rem;
      
      h3 {
        margin-top: 1rem;
        margin-left: 0.125rem;
        font-size: 1rem;
        font-family: zeiss;
        font-weight: 400;
        b{
          font-size: 1;
          font-family: zeissBold;
        }
      }
    }
  }

  .isLoading {
    min-width: 100%;
    opacity: 0.35;
    display: flex;
    align-items: center;
    justify-content: flex-start;
    padding-bottom: 1rem;
  }

  .rowDiv {
    display: flex;
    flex-direction: column;

    .isLoading {
      min-width: 100%;
      opacity: 0.35;
      display: flex;
      align-items: center;
      justify-content: flex-start;
      padding-bottom: 1rem;
    }
  }

  @media only screen and (min-width: 1024px) {
    background-color: #FFFFFF;
    box-shadow: 0rem 0.25rem 0rem 0rem #e4ebf2;
    padding: 2rem;
    margin-top: 2.5rem;
    border-radius: 0.25rem;
    border: solid 0.0625rem #c1cbd9;
    .line {
      margin-top: 1.5rem;
      margin-bottom: 1rem;
      gap: 1.5rem;
    }

    .rowDiv {
      width: 100%;
      display: flex;
      flex-direction: row;
      align-items: center;
      justify-content: center;
    }

    .paciente {
      display: flex;
      flex-direction: row;
      .separator {
        width: 3rem;
      }
    }
    .medico {
      display: flex;
      flex-direction: row;
      .separator {
        width: 3rem;
      }
    }
  }
`

export const DivMargin = styled.div`
  height: 0.5rem;
`
