import MZeissLogo from '@/assets/svg/mZ.svg'
import ZeissLogo from '@/assets/svg/zeissLogo.svg'
import * as Styles from './styles'

export function Header() {
  return (
    <Styles.HeaderContainer>
      <div className="wrapper">
        <div className="titleWrapper" >
          <MZeissLogo className="mzeissLogo" />
        </div>
          <h1 className="title">LENS WIPES</h1>
        <ZeissLogo />
      </div>
    </Styles.HeaderContainer>
  )
}
