import styled from 'styled-components'

export const HeaderContainer = styled.div`
  position: fixed;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: center;
  padding: 1rem 1.25rem 0rem;
  max-height: 14rem;
  top: 0rem;
  right: 0rem;
  background-color: #ffffff;
  /* min-height: 9rem; */
  z-index: 8;
  width: 100%;
  box-shadow: 0rem 0.25rem 1rem rgba(0, 0, 0, 0.06);
  h1 {
    font-size: 1.5rem;
    font-weight: 400;
  }

  .wrapper {
    width: 100%;
    max-width: 90rem;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;

    .titleWrapper {
        display: flex;
        flex-direction: row;
        align-items: center;
    }
  }
  .title {
        color: #353D45;
    }
  @media only screen and (min-width: 1024px) {
    min-height: 9.5rem;

    max-height: 9.5rem;
    padding-top: 0rem;
    flex-direction: row;
    padding-left: 5rem;
    padding-right: 5rem;
    justify-content: center;
    align-items: center;
    box-shadow: 0rem 0.25rem 1rem rgba(0, 0, 0, 0.06);
    .title {
        color: #353D45;
    }
    .mzeissLogo {
        margin-right: 5rem;
    }

    @media only screen and (max-width: 1200px) {
    }
  }
`
