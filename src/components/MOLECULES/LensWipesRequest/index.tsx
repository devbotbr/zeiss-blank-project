import BackIcon from '@/assets/svg/primary_back.svg'
import InfoIcon from '@/assets/svg/infoIc.svg'
import NextArrow from '@/assets/svg/nextArrow.svg'
import {
  CustomDropdownSearch,
  FormLensWipe,
  InputCheckbox,
  MaterialInput,
  PrimaryButton,
} from '@/components'
import { useGlobal } from '@/providers/global'
import { LabeledText } from '@/components'
import { useForm } from 'react-hook-form'
import { useEffect, useState } from 'react'
import * as Styles from './styles'
import { useRegister } from '@/providers/register'
import { TConsultaCNPJ } from '@/types'
import { Spin } from 'antd'

export function LensWipesRequest() {


  return (
    <Styles.Wrapper>
      <h1>Novo pedido de Lens Wipes</h1>
      <div className='row'>
        <FormLensWipe />
        <Styles.ImageSpace />
      </div>
    </Styles.Wrapper>
  )
}
