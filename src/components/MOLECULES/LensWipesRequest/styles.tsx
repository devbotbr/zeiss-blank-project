import styled, { keyframes } from 'styled-components'

const FadeIn = keyframes`
  from {
    opacity: 0;
  }
  to {
    opacity: 1;
  }
`

export const Wrapper = styled.div`
  width: 100%;
  min-height: 50vh;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  animation: ${FadeIn} 1.5s;

  .row {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    gap: 5.6rem;
  }

  h1 {
    color:#353D45;
    font-family: zeiss;
    font-size: 1.75rem;
    font-style: normal;
    font-weight: 300;
    line-height: 100%; /* 1.75rem */
  }
`

export const ImageSpace = styled.div`
  margin-top: 2.5rem;
  min-width: 21.65194rem;
  min-height: 30.75rem;
  max-width: 21.65194rem;
  max-height: 30.75rem;
  background-image: url('/images/lenswipes.png');
`