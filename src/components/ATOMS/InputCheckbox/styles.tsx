import styled from 'styled-components'
import { TInputCheckbox } from '@/types/ATOMS/InputCheckbox'

export const Content = styled.div<TInputCheckbox>`
  display: ${(props) => (props.isVisible === 'false' ? 'none' : 'flex')};
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  flex-wrap: nowrap;
  //padding: 1rem;
  margin: ${(props) => props.marginMobile ?? '0.5rem 0 1rem 0'};
  border: 1px solid #c1cbd9;
  border-radius: 0.125rem;
  height: 3.5rem;
  width: ${(props) => props.widthMobile ?? '100%'};
  svg {
    margin-right: 0.5rem;
  }

  .switch {
    padding-left: 1rem;
    padding-right: 1rem;
    border-left: solid 0.0625rem #dde3ed;
    .swtc {
      display: flex;
      align-items: center;
      justify-content: center;
      padding-bottom: 0.125rem;
      height: max-content;
      width: max-content;
      max-height: 1.75rem;
      border-radius: 10rem;
    }
  }
  .label {
    display: flex;
    flex-direction: row;
    p {
      color: #C1CBD9;
      padding-left: 1rem;
      font-size: 1rem;
      line-height: 1rem;
      font-weight: 400;
    }
    span {
      font-family: zeissBold;
      font-size: 1rem;
      line-height: 1rem;
      font-weight: 700;
      padding-top: 0.25rem;
      padding-left: 0.25rem;
    }
  }
  @media only screen and (min-width: 1024px) {
    width: ${(props) => (props.width ? props.width : '100%')};
    margin: ${(props) => props.margin ?? '0.5rem 0 1rem 0'};
  }
`

interface SwitchWrapperProps {
  checked: boolean
}

interface SwitchSliderProps {
  checked: boolean
}

export const SwitchWrapper = styled.div<SwitchWrapperProps>`
  display: inline-block;
  width: 3rem;
  height: 1.75rem;
  background-color: ${({ checked }) => (checked ? '#0050F2' : '#E5E5EA')};
  border-radius: 0.875rem;
  cursor: pointer;
  transition: background-color 0.3s ease;
  margin-top: 0.25rem;
  &:hover {
    opacity: 0.8;
  }
`

export const SwitchSlider = styled.span<SwitchSliderProps>`
  position: relative;
  display: inline-block;
  width: 1.5rem;
  height: 1.5rem;
  margin-top: 0.125rem;
  background-color: white;
  border-radius: 0.75rem;
  transition: transform 0.3s ease;
  transform: translateX(
    ${({ checked }) => (checked ? '1.375rem' : '0.125rem')}
  );
`

export const SwitchInput = styled.input`
  display: none;
`
