/* eslint-disable no-unused-expressions */
import { TInputCheckbox } from '@/types/ATOMS/InputCheckbox'

import Icon from '@/assets/icons/glasses.svg'
import { CupertinoSwitch } from '@/components'
import { useState } from 'react'
import * as Styles from './styles'

export function InputCheckbox({
  label,
  subLabel,
  width,
  widthMobile,
  margin,
  marginMobile,
  value,
  setStateProp,
  disabled
}: TInputCheckbox) {
  const [checked, setChecked] = useState(value ?? false)

  const handleToggle = () => {
    if (!disabled || checked) {
      const newValue = !checked
      setChecked(newValue)
      setStateProp?.(newValue)
    }
  }

  return (
    <Styles.Content
      width={width}
      widthMobile={widthMobile}
      margin={margin}
      marginMobile={marginMobile}
    >
      <div className="label">
        <p>{label}</p>
        {subLabel && <span>{subLabel}</span>}
      </div>

      <div className="switch">
        <div className="swtc" aria-hidden>
          <Styles.SwitchWrapper checked={value!} onClick={handleToggle}>
            <Styles.SwitchSlider checked={value!} />
            <Styles.SwitchInput
              type="checkbox"
              checked={value}
              onChange={handleToggle}
            />
          </Styles.SwitchWrapper>
        </div>
      </div>
    </Styles.Content>
  )
}
