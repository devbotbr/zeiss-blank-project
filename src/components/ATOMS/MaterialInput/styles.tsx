/* eslint-disable no-nested-ternary */
/* eslint-disable no-unused-expressions */
import { MaterialInputType } from '@/types'
import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  position: relative;
`

export const InputWrapper = styled.div<MaterialInputType>`
  position: relative;
  display: ${(props) => (props.isVisible === 'false' ? 'none' : 'flex')};
  flex-direction: row;
  align-items: center;
  /* width: ${(props) => props.width ?? '100%'}; */
  width: 100%;
  height: ${(props) => (!props.textArea ? '3.5rem' : '7.5rem')};
  border: solid 0.0625rem
    ${(props) =>
      props.error ? '#E04747' : props.isFocused ? '#353D45' : '#a5b1c2'};
  margin: ${(props) => props.marginMobile ?? '1.5rem 0rem 1.5rem 0rem'};
  border-radius: 0.125rem;
  background-color: ${(props) => (props.disabled ? '#F5F7FA' : '#ffffff')};
  .divIcon {
    display: flex;
    justify-content: center;
    align-items: center;
    margin: 1rem 0.5rem 1rem 1rem;
  }
  .divSufixIcon {
    display: flex;
    justify-content: center;
    align-items: center;
    margin: 1rem 1rem 1rem 1rem;
  }
  p {
    font-size: 1rem;
    font-weight: 400;
    color: #353d45;
    margin-right: 1rem;
  }

  .label {
    position: absolute;
    top: -0.4rem;
    left: 0.5rem;
    font-size: 0.75rem;
    line-height: 0.75rem;

    background-color: ${(props) => (!props.color ? '#f6f7fa' : props.color)};
    padding: 0rem 0.25rem;
    color: ${(props) =>
      props.error ? '#E04747' : props.isFocused ? '#353D45' : '#a5b1c2'};
    font-weight: 400;
    text-align: left;
    transition: color 0.75s;

    @media only screen and (min-width: 1024px) {
      background-color: #ffffff;
    }
  }
  input {
    height: 100%;
    width: 100%;
    border: none;
    padding: 0.875rem ${(props) => (props.icon ? '0rem' : '1rem')};
    color: #353d45;
    border-radius: 0.125rem;
    font-size: 0.875rem;
    background-color: transparent;
    outline: none;
    letter-spacing: ${(props) =>
      props.type === 'password' ? '0.125rem' : null};

    :focus {
      color: #000000;
      outline: none;
    }
    ::placeholder {
      color: #a5b1c2;
      letter-spacing: normal;
    }
  }
  input::-webkit-outer-spin-button,
  input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
  input[type='number'] {
    -moz-appearance: textfield;
  }
  textarea {
    height: 100%;
    width: 100%;
    border: none;
    padding: 0.875rem;
    color: #828d9e;
    border-radius: 0.125rem;
    font-size: 0.875rem;
    resize: none;
    background-color: ${(props) => (!props.color ? '#FFFFFF' : props.color)};
    :focus {
      color: '#000000';
    }
  }

  @media only screen and (min-width: 1024px) {
    margin: ${(props) => props.margin ?? '1.5rem 0rem 1.5rem 0rem'};
    width: ${(props) => props.width ?? '100%'};

    /* max-width: 24.25rem; */
  }
`

export const Error = styled.div`
  position: absolute;
  top: 3rem;
  right: 0;
  height: max-content;
  span {
    font-size: 0.6875rem;
    color: #e04747;
    width: 100%;
    font-weight: 400;
    :hover {
      cursor: text;
    }
  }
`
