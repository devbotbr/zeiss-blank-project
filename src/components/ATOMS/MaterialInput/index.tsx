import { MaterialInputType } from '@/types'
import { useState } from 'react'
import * as Styles from './styles'

export function MaterialInput({
  label,
  icon,
  register,
  rightSideIcon,
  type,
  width,
  placeholder,
  textArea,
  color,
  sufix,
  margin,
  marginMobile,
  sufixIcon,
  defaultValue,
  setStateProp,
  error,
  disabled,
  onChange,
  isVisible,
  onFocusedSetIndex,
  mask,
}: MaterialInputType) {
  const [isInputFocused, setIsInputFocused] = useState<boolean>(false)
  const [fieldValidation, setFieldValidation] = useState<boolean>(false)

  return (
    <Styles.InputWrapper
      onFocus={(f) => {
        setIsInputFocused(true)
      }}
      onBlur={() => {
        setIsInputFocused(false)
      }}
      onChangeCapture={(e) => {
        if (error !== '') {
          setFieldValidation(true)
        }
      }}
      label={label}
      icon={icon}
      register={register}
      rightSideIcon={rightSideIcon}
      placeholder={placeholder}
      width={width}
      type={type}
      textArea={textArea}
      color={color}
      isFocused={isInputFocused}
      sufix={sufix}
      sufixIcon={sufixIcon}
      margin={margin}
      marginMobile={marginMobile}
      defaultValue={defaultValue}
      setStateProp={setStateProp}
      error={error && fieldValidation ? error : ''}
      disabled={disabled}
      isVisible={isVisible}
    >
      <h1 className="label">{label}</h1>
      {icon ? <div className="divIcon">{icon}</div> : null}
      {textArea ? (
        <textarea placeholder={placeholder} onChange={onChange} />
      ) : (
        <input
          placeholder={placeholder}
          type={type}
          autoComplete="off"
          onFocus={onFocusedSetIndex}
          {...register}
          defaultValue={defaultValue}
          onBlur={setStateProp}
          disabled={disabled}
        />
      )}
      <p>{sufix}</p>
      {sufixIcon ? <div className="divSufixIcon">{sufixIcon}</div> : null}
      {error && fieldValidation ? (
        <Styles.Error>
          <span>{error}</span>
        </Styles.Error>
      ) : null}
    </Styles.InputWrapper>
  )
}
