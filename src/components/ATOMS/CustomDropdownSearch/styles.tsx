import { TCustomDropdown } from '@/types'
import styled, { css, keyframes } from 'styled-components'

const ArrowAnimationOpen = keyframes`
    from{
        transform: rotateZ(0deg)
    }
    to{
        transform: rotateZ(180deg)
    }
`
const ArrowAnimationClose = keyframes`
    from{
        transform: rotateZ(180deg)
    }
    to{
        transform: rotateZ(0deg)
    }
`

type TCustomDropdownController = {
  isOpen: boolean
}

export const Wrapper = styled.div<TCustomDropdown>`
  position: relative;
  display: ${(props) => (props.isVisible === 'false' ? 'none' : 'flex')};
  flex-direction: row;
  width: ${(props) => props.widthMobile ?? '100%'};
  height: 3.5rem;
  padding: 0rem 1rem;
  align-items: center;
  gap: 0.5rem;
  flex-shrink: 0;
  border-radius: 0.125rem;
  border: 0.0625rem solid #828d9e;
  background: ${(props) => (props.disabled ? '#F5F7FA' : '#ffffff')};
  margin: ${(props) => props.marginMobile ?? '1rem 0rem'};
  cursor: pointer;
  .title {
    position: absolute;
    top: -0.4rem;
    left: 0.5rem;
    font-size: 0.75rem;
    line-height: 0.75rem;
    background-color: ${(props) => (!props.color ? '#f6f7fa' : props.color)};
    padding: 0rem 0.25rem;
    font-weight: 400;
    text-align: left;
    transition: color 0.75s;

    @media only screen and (min-width: 1024px) {
      background-color: #ffffff;
    }
  }
  form {
    width: 100%;
  }
  .arrow {
    ${(props) =>
      props.isOpen
        ? css`
            animation: ${ArrowAnimationOpen} 0.2s linear;
            transform: rotateX(180deg);
          `
        : css`
            animation: ${ArrowAnimationClose} 0.2s linear;
            transform: rotateX(0deg);
          `};
  }

  .searchBox {
    width: 100%;
    background-color: transparent;
    outline-color: transparent;
    border: none;
    :focus {
      outline: none;
    }
    ::placeholder {
      color: #a5b1c2;
    }
  }

  @media only screen and (min-width: 1024px) {
    //max-width: 48.5%;
    margin: ${(props) => props.margin ?? '0rem'};
    width: ${(props) => props.width ?? '100%'};
  }
`

export const ListItems = styled.div<TCustomDropdownController>`
  position: absolute;
  width: 100%;
  display: ${(props) => (props.isOpen ? 'flex' : 'none')};
  flex-direction: column;
  align-items: flex-start;
  min-height: 3rem;
  max-height: 16rem;
  left: 0;
  top: 3.75rem;
  z-index: 15;
  background-color: #ffffff;
  border-radius: 0.125rem;
  border: 0.0625rem solid #828d9e;
  box-shadow: 0px 0px 24px 0px rgba(0, 0, 0, 0.2);
  overflow-y: scroll;
  overflow-x: hidden;

  p {
    position: relative;
    width: 100%;
    display: flex;
    min-height: 3rem;
    max-height: 3rem;
    padding: 0rem 1rem;
    flex-direction: column;
    justify-content: center;
    align-items: flex-start;
    align-self: stretch;
  }
`
