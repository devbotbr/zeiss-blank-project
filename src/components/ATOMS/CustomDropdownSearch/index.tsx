/* eslint-disable no-unused-expressions */
/* eslint-disable prefer-const */
/* eslint-disable no-param-reassign */
import React, { useEffect, useState } from 'react'
import Arrow from '@/assets/svg/arrow.svg'

import { TCustomDropdown } from '@/types'
import { useForm } from 'react-hook-form'
import { randomInt } from 'crypto'
import * as Styles from './styles'

export function CustomDropdownSearch({
  label,
  options,
  search,
  placeholder,
  prefixIcon,
  margin,
  marginMobile,
  width,
  widthMobile,
  id,
  defaultValue,
  state,
  disabled,
  isVisible,
}: TCustomDropdown) {
  const [isOpen, setIsOpen] = useState<boolean>(false)
  const [list, setList] = useState(options)
  const { watch, register, setValue } = useForm()
  const searchData = watch('search')
  let counter = 0

  useEffect(() => {}, [searchData])

  const handleSearch = (event: any) => {
    const searchTerm = event.target.value
    const filteredList = options.filter((item) =>
      item.toLowerCase().includes(searchTerm.toLowerCase()),
    )
    setList(filteredList)
  }

  return (
    <Styles.Wrapper
      label={label}
      options={options}
      search={search}
      placeholder={placeholder}
      isOpen={isOpen}
      margin={margin}
      marginMobile={marginMobile}
      width={width}
      widthMobile={widthMobile}
      disabled={options.length === 0 ? true : disabled}
      onClick={() => {
        if (!disabled) {
          setIsOpen(!isOpen)

          if (!isOpen) {
            document.getElementById(id!)?.focus()
          }
        }
      }}
      isVisible={isVisible}
    >
      <h1 className="title">{label}</h1>
      {prefixIcon}
      <form>
        <input
          id={id}
          className="searchBox"
          autoComplete="off"
          defaultValue={defaultValue}
          placeholder={placeholder}
          disabled={!search}
          onClick={(event) => {
            if (event.currentTarget.value !== '') {
              event.currentTarget.value = ''
              setValue('search', '')
              handleSearch(event)
            }
          }}
          {...register('search', {
            onChange: handleSearch,
          })}
        />
      </form>
      <Arrow className="arrow" />
      <Styles.ListItems isOpen={isOpen}>
        {list.map((item) => {
          counter += 1
          return (
            <p
              aria-hidden
              key={counter}
              onClick={() => {
                setValue('search', item)

                state!(item)
              }}
            >
              {item}
            </p>
          )
        })}
      </Styles.ListItems>
    </Styles.Wrapper>
  )
}
