import styled from 'styled-components'

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  width: 18.875rem;
  margin-right: 1rem;
  h4 {
    color: #828d9e;
    font-size: 0.75rem;
    font-style: normal;
    font-weight: 400;
    line-height: 0.75rem;
  }
  .content {
    color: #353d45;
    font-size: 1rem;
    font-style: normal;
    font-family: zeissBold;
    font-weight: 400;
    line-height: normal;
    margin-left: 0.125rem;
  }
`
