import { TLabeledText } from '@/types'
import * as Styles from './styles'

export function LabeledText({title, content}:TLabeledText) {
  return <Styles.Wrapper>
    <h4>{title}</h4>
    <p className='content'>{content}</p>
  </Styles.Wrapper>
}
