import { TPrimaryButton } from '@/types'
import * as Styles from './styles'

export function PrimaryButton({
  label,
  disabled,
  onPressed,
  preffixIcon,
  suffixIcon,
  width,
}: TPrimaryButton) {
  return (
    <Styles.Wrapper
      disabled={disabled}
      onClick={!disabled ? onPressed : () => {}}
      label={label}
      width={width}
    >
      {preffixIcon}
      <p>{label}</p>
      {suffixIcon}
    </Styles.Wrapper>
  )
}
