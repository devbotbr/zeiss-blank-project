import { TPrimaryButton } from '@/types'
import styled from 'styled-components'

export const Wrapper = styled.div<TPrimaryButton>`
  position: relative;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  min-height: 3rem;
  max-height: 3rem;
  /* padding: 1rem; */
  width: ${(props) => props.width ?? '19.9345rem'};
  background-color: ${(props) => (props.disabled ? '#7aa3f6' : '#0050F2')};
  border: solid 0.0625rem transparent;
  border-radius: 0.125rem;
  cursor: ${(props) => (props.disabled ? 'not-allowed' : 'pointer')};
  transition: background 0.35s;
  padding: 1rem;

  p {
    text-align: center;
    line-height: 1rem;
    margin: 0rem;
    padding: 0rem;
    color: #ffffff;
    font-size: 1rem;
    margin-right: 0.75rem;
  }
  svg {
    fill: #ffffff;
  }

  :hover {
    background-color: ${(props) =>
      props.disabled ? '#7aa3f6' : '#FFFFFF'} !important;
    border: solid 0.0625rem #0050f2;
    svg {
      fill: ${(props) => (props.disabled ? '#FFFFFF' : '#0050f2')};
    }
    p {
      color: ${(props) => (props.disabled ? '#FFFFFF' : '#0050f2')};
    }
  }
`
