/* eslint-disable react/jsx-no-undef */

import {
 ConfirmationModal
} from '@/components'
import { useGlobal } from '@/providers/global/'
import { useState } from 'react'
import * as Styles from './styles'

export function Modal() {
  const { modalType, isModalOpen } = useGlobal()

  const [isOpen, setIsOpen] = useState<boolean>(isModalOpen)

  const modalsList = [
    <ConfirmationModal key='0' />
  ]

  return (
    <Styles.ModalContainer
      backgroundModalColor="rgba(0, 80, 242, 0.7)"
      isOpen={isModalOpen}
    >
      {modalsList[modalType]}
    </Styles.ModalContainer>
  )
}
