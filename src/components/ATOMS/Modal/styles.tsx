import { ModalProps } from '@/types'
import styled, { keyframes } from 'styled-components'

const fadeIn = keyframes`
  from{
    opacity: 0;
  }
  to{
    opacity: 1;
  }
`

export const ModalContainer = styled.div<ModalProps>`
  height: 100vh;
  width: 100vw;
  position: fixed;
  animation: ${fadeIn} 0.35s;
  /* cursor: pointer; */
  background-color: ${(props) =>
    !props.backgroundModalColor
      ? 'rgba(0, 80, 242, 0.7)'
      : props.backgroundModalColor};
  backdrop-filter: blur(1rem);
  z-index: 16;
  display: ${(props) => (props.isOpen ? 'flex' : 'none')};
  justify-content: center;
  align-items: center;
  flex-direction: column;
  overflow-y: hidden;
  overflow-x: hidden;

  .modalWrapper {
    position: relative;
    width: 100%;
  }
  .closeIcon {
    position: absolute;
    top: -3rem;
    right: 45%;
    cursor: pointer;
    fill: #FFFFFF;
    height: 2rem;
    width: 2rem;
  }

  @media only screen and (max-width: 768px) {
    .closeIcon {
      top: -2rem;
      right: 45%;
    }
  }
`
