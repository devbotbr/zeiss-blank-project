import styled from 'styled-components'

export const LabelText = styled.h2`
color: #828D9E;
font-family: zeiss;
font-size: 1.125rem;
font-style: normal;
font-weight: 400;
line-height: normal;
`
