import { TLabelText } from '@/types'
import * as Styles from './styles'

export function LabelText({ label }: TLabelText) {
  return <Styles.LabelText>{label}</Styles.LabelText>
}
