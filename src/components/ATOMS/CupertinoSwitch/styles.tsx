import styled from 'styled-components'

interface SwitchWrapperProps {
  checked: boolean
}

interface SwitchSliderProps {
  checked: boolean
}

export const SwitchWrapper = styled.div<SwitchWrapperProps>`
  display: inline-block;
  width: 3rem;
  height: 1.75rem;
  background-color: ${({ checked }) => (checked ? '#0050F2' : '#E5E5EA')};
  border-radius: 0.875rem;
  cursor: pointer;
  transition: background-color 0.3s ease;
  margin-top: 0.25rem;
  &:hover {
    opacity: 0.8;
  }
`

export const SwitchSlider = styled.span<SwitchSliderProps>`
  position: relative;
  display: inline-block;
  width: 1.5rem;
  height: 1.5rem;
  margin-top: 0.125rem;
  background-color: white;
  border-radius: 0.75rem;
  transition: transform 0.3s ease;
  transform: translateX(
    ${({ checked }) => (checked ? '1.375rem' : '0.125rem')}
  );
`

export const SwitchInput = styled.input`
  display: none;
`
