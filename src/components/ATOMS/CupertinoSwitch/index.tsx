import { useState } from 'react'
import { SwitchWrapper, SwitchSlider, SwitchInput } from './styles'

type TCupertinoButton = {
  defaultChecked: boolean
}

export function CupertinoSwitch({ defaultChecked }: TCupertinoButton) {
  const [checked, setChecked] = useState(defaultChecked ?? false)

  const handleToggle = () => {
    setChecked(!checked)
  }

  return (
    <SwitchWrapper checked={checked} onClick={handleToggle}>
      <SwitchSlider checked={checked} />
      <SwitchInput type="checkbox" checked={checked} onChange={handleToggle} />
    </SwitchWrapper>
  )
}
